/* Dooba SDK
 * ESP8266 WiFi Module Driver
 */

#ifndef	__ESP8266_H
#define	__ESP8266_H

// External Includes
#include <stdint.h>
#include <avr/io.h>
#include <net/ip.h>
#include <net/eth.h>
#include <net/wifi.h>
#include <socket/iface.h>

// Define Defaults
#ifndef	ESP8266_BAUDRATE
#define	ESP8266_BAUDRATE				9600UL
#endif

// UART Prescaler
#define	ESP8266_PRESCALE				((F_CPU / (ESP8266_BAUDRATE * 16UL)) - 1)

// Delays
#define	ESP8266_DELAY_RST				500
#define	ESP8266_DELAY_START				1000
#define	ESP8266_MSG_PROCESS_DELAY		10

// Modes
#define	ESP8266_MODE_STA				1
#define	ESP8266_MODE_AP					2
#define	ESP8266_MODE_APSTA				3

// Channels
#define	ESP8266_CHANNELS				4

// Responses
#define	ESP8266_RESPONSE_NONE			0
#define	ESP8266_RESPONSE_OK				1
#define	ESP8266_RESPONSE_ERROR			2

// Security Types
#define	ESP8266_SEC_OPEN				0
#define	ESP8266_SEC_WEP					1
#define	ESP8266_SEC_WPA_PSK				2
#define	ESP8266_SEC_WPA2_PSK			3
#define	ESP8266_SEC_WPA_WPA2_PSK		4

// Channel Types
#define	ESP8266_CHAN_TCP				0
#define	ESP8266_CHAN_UDP				1

// UDP Peer Modes
#define	ESP8266_UDP_PEERNOCHANGE		0
#define	ESP8266_UDP_PEERCHANGEONCE		1
#define	ESP8266_UDP_PEERCHANGEMULTI		2

// Response Timeout (ms)
#define	ESP8266_RESPONSE_TIMEOUT		2000

// SCOM Buffer Size
#define	ESP8266_SCOM_BUFSIZE			8

// Access Point Structure
struct esp8266_ap
{
	// Name
	char name[NET_WIFI_AP_NAME_LEN];

	// Name Length
	uint8_t name_len;

	// Security Type
	uint8_t sec_type;

	// Channel
	uint8_t chan;

	// Signal Quality
	uint8_t q;
};

// Access Point Client Structure
struct esp8266_ap_client
{
	// IP Address
	char ip[NET_IP_ADDR_LEN_TXT];
};

// Socket Interface
extern struct socket_iface esp8266_socket_iface;

// Initialize
extern void esp8266_init(uint8_t *rxbuf, uint16_t rxbuf_size, uint8_t en_pin, uint8_t rst_pin, uint8_t flash_pin);

// Re-Initialize (reset + init with current params)
extern void esp8266_reinit();

// Update
extern void esp8266_update();

// Hard Reset
extern void esp8266_reset();

// Soft Reset
extern void esp8266_soft_reset();

// Enter Flash Mode
extern void esp8266_enter_flash();

// Enter Passthrough Mode
extern void esp8266_enter_passthru();

// Has Message? (Returns next)
extern uint8_t esp8266_has_msg(uint16_t start, uint16_t len, uint16_t *nxt_start, uint16_t *nxt_len);

// Consume Message
extern void esp8266_consume_msg(uint16_t start, uint16_t len);

// Wait for Command completion
extern uint8_t esp8266_wait_for_res();

// Wait for Command completion while dropping channel events
extern uint8_t esp8266_wait_for_res_drop_events(uint8_t chan);

// Set Mode
extern uint8_t esp8266_set_mode(uint8_t mode);

// Set Mux
extern uint8_t esp8266_set_mux(uint8_t mux);

// List Access Points
extern uint8_t esp8266_list_ap(struct esp8266_ap *ap_list, uint8_t ap_list_size, uint8_t *count);

// Join Access Point
extern uint8_t esp8266_join_ap(void *name, void *password);

// Join Access Point - Fixed Length
extern uint8_t esp8266_join_ap_n(void *name, uint8_t name_l, void *password, uint8_t password_l);

// Create Access Point
extern uint8_t esp8266_create_ap(void *name, void *password, uint8_t sec_type, uint8_t wchan);

// Create Access Point - Fixed Length
extern uint8_t esp8266_create_ap_n(void *name, uint8_t name_l, void *password, uint8_t password_l, uint8_t sec_type, uint8_t wchan);

// List AP Clients
extern uint8_t esp8266_list_ap_clients(struct esp8266_ap_client *client_list, uint8_t client_list_size, uint8_t *count);

// Set AP IP Address
extern uint8_t esp8266_set_ap_addr(void *ip);

// Set AP IP Address - Fixed Length
extern uint8_t esp8266_set_ap_addr_n(void *ip, uint8_t ip_l);

// Get AP IP Address (IP Buffer must be at least 16 bytes)
extern uint8_t esp8266_get_ap_addr(char *ip);

// Get Local MAC & IP Address (MAC buffer must be 6 bytes, IP Buffer must be at least 16 bytes)
extern uint8_t esp8266_get_local_info(uint8_t *mac, char *ip);

// Open UDP (Socket Interface)
extern uint8_t esp8266_iface_open_udp(void *ifdata, struct socket **s, char *host, uint8_t host_l, uint16_t port);

// Connect (Socket Interface)
extern uint8_t esp8266_iface_cnct(void *ifdata, struct socket **s, char *host, uint8_t host_l, uint16_t port);

// Listen (Socket Interface)
extern uint8_t esp8266_iface_lstn(void *ifdata, struct socket **s, uint16_t port);

// Send (Socket Interface)
extern uint8_t esp8266_iface_send(void *ifdata, struct socket *s, uint8_t *data, uint16_t size);

// Get Peer (Socket Interface)
extern uint8_t esp8266_iface_get_peer(void *ifdata, struct socket *s, char *ip, uint16_t *port);

// Close (Socket Interface)
extern void esp8266_iface_close(void *ifdata, struct socket *s);

// Send data over channel
extern uint8_t esp8266_send_channel(uint8_t channel, void *x, uint16_t s);

// Send Command - Format
extern void esp8266_send_cmd_f(char *fmt, ...);

// Format String Printer
extern void esp8266_send_cmd_f_print(void *user, uint8_t c);

// Send Command
extern void esp8266_send_cmd(char *cmd);

// Send Data
extern void esp8266_send(uint8_t *d, uint16_t s);

// Send Byte
extern void esp8266_write(uint8_t d);

// Handle Access Point Information
extern void esp8266_handle_ap(uint16_t s, uint16_t l);

// Handle Access Point Client Information
extern void esp8266_handle_ap_client(uint16_t s, uint16_t l);

// Handle Access Point IP Information
extern void esp8266_handle_ap_ip(uint16_t s, uint16_t l);

// Handle Peer Info
extern void esp8266_handle_peer(uint16_t s, uint16_t l);

// Handle Local MAC Information
extern void esp8266_handle_local_mac(uint16_t s, uint16_t l);

// Handle Local IP Information
extern void esp8266_handle_local_ip(uint16_t s, uint16_t l);

// Handle Connect
extern void esp8266_handle_cnct(uint16_t s, uint16_t l);

// Handle Disconnect
extern void esp8266_handle_dcnt(uint16_t s, uint16_t l);

// Handle IP Data
extern void esp8266_handle_ip_data(uint16_t s, uint16_t l);

// SCOM RX Handler
extern void esp8266_scom_rx_handler(uint8_t x);

#endif
