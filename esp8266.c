/* Dooba SDK
 * ESP8266 WiFi Module Driver
 */

// External Includes
#include <string.h>
#include <stdlib.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <dio/dio.h>
#include <scom/scom.h>
#include <util/str.h>
#include <util/isafe.h>
#include <util/cprintf.h>

// Internal Includes
#include "esp8266.h"

// Pins
uint8_t esp8266_pin_en;
uint8_t esp8266_pin_rst;
uint8_t esp8266_pin_flash;

// Serial Passthrough
uint8_t esp8266_passthru;

// First Update
uint8_t esp8266_first_update;

// Receive Buffer
uint8_t *esp8266_rxbuf;
uint16_t esp8266_rxbuf_size;
uint16_t esp8266_rxbuf_pos;
uint8_t esp8266_overflow;

// Protection Area
uint16_t esp8266_prot_s;
uint16_t esp8266_prot_l;

// AP / Client Lists
struct esp8266_ap *esp8266_ap_list;
uint8_t esp8266_ap_list_size;
uint8_t esp8266_ap_list_count;
struct esp8266_ap_client *esp8266_ap_client_list;
uint8_t esp8266_ap_client_list_size;
uint8_t esp8266_ap_client_list_count;

// Get Peer Data
char *esp8266_get_peer_ip;
uint16_t *esp8266_get_peer_port;
uint8_t esp8266_get_peer_sid;

// Sockets
struct socket esp8266_sockets[ESP8266_CHANNELS];
struct socket esp8266_server_socket;

// Local Address Buffers
uint8_t *esp8266_local_mac_buf;
char *esp8266_local_ip_buf;

// I/O Control
uint8_t esp8266_response;
uint8_t esp8266_sending;

// Socket Interface
struct socket_iface esp8266_socket_iface;

// SCOM Buffer
uint8_t esp8266_scom_buf[ESP8266_SCOM_BUFSIZE];

// Initialize
void esp8266_init(uint8_t *rxbuf, uint16_t rxbuf_size, uint8_t en_pin, uint8_t rst_pin, uint8_t flash_pin)
{
	uint8_t i;

	// Set Pins
	esp8266_pin_en = en_pin;
	esp8266_pin_rst = rst_pin;
	esp8266_pin_flash = flash_pin;

	// Set First Update
	esp8266_first_update = 1;

	// Set RX Buffer
	esp8266_rxbuf = rxbuf;
	esp8266_rxbuf_size = rxbuf_size;
	esp8266_rxbuf_pos = 0;

	// Clear Protection Area
	esp8266_prot_s = 0;
	esp8266_prot_l = 0;

	// Clear Buffers
	esp8266_local_mac_buf = 0;
	esp8266_local_ip_buf = 0;

	// Clear Lists
	esp8266_ap_list = 0;
	esp8266_ap_list_size= 0;
	esp8266_ap_list_count = 0;
	esp8266_ap_client_list = 0;
	esp8266_ap_client_list_size = 0;
	esp8266_ap_client_list_count = 0;

	// Clear Get Peer Data
	esp8266_get_peer_ip = 0;
	esp8266_get_peer_port = 0;
	esp8266_get_peer_sid = 0;

	// Clear Sockets
	for(i = 0; i < ESP8266_CHANNELS; i = i + 1)																		{ esp8266_sockets[i].iface = 0; }

	// Set Server Inactive
	esp8266_server_socket.iface = 0;

	// Clear Overflow
	esp8266_overflow = 0;

	// Enable UART Power
	PRR0 &= ~(1 << PRUSART1);

	// Load up Prescaler
	UBRR1H = (ESP8266_PRESCALE >> 8);
	UBRR1L = ESP8266_PRESCALE & 0xff;

	// Enable UART RX & TX
	UCSR1A = 0;
	UCSR1B = (1 << RXEN1) | (1 << TXEN1) | (1 << RXCIE1);
	UCSR1C = (1 << UCSZ11) | (1 << UCSZ10);

	// Configure Pins
	dio_output(esp8266_pin_en);
	dio_output(esp8266_pin_rst);
	dio_output(esp8266_pin_flash);
	dio_hi(esp8266_pin_flash);
	dio_hi(esp8266_pin_en);
	dio_hi(esp8266_pin_rst);
	_delay_ms(ESP8266_DELAY_RST);

	// Enable ESP8266
	dio_lo(esp8266_pin_rst);
	_delay_ms(ESP8266_DELAY_RST);
	dio_hi(esp8266_pin_rst);
	_delay_ms(ESP8266_DELAY_RST);

	// Disable Passthru
	esp8266_passthru = 0;

	// Clear Response
	esp8266_response = ESP8266_RESPONSE_NONE;

	// Not Sending
	esp8266_sending = 0;

	// Initialize & Register Socket Interface
	socket_iface_init(&esp8266_socket_iface, 0, esp8266_iface_open_udp, esp8266_iface_cnct, esp8266_iface_lstn, esp8266_iface_send, esp8266_iface_get_peer, esp8266_iface_close, "espwifi");
	socket_register_iface(&esp8266_socket_iface);

	// Disable Echo (And make sure we don't get that first 'OK')
	esp8266_send((uint8_t *)"ATE0\r\n", 6);
	_delay_ms(ESP8266_DELAY_RST);
	esp8266_send((uint8_t *)"ATE0\r\n", 6);
	_delay_ms(ESP8266_DELAY_START);
	i = UDR1;
	i = UDR1;
	i = UDR1;
	esp8266_rxbuf_pos = 0;
}

// Re-Initialize (reset + init with current params)
void esp8266_reinit()
{
	// Reset
	esp8266_reset();

	// Init
	esp8266_init(esp8266_rxbuf, esp8266_rxbuf_size, esp8266_pin_en, esp8266_pin_rst, esp8266_pin_flash);
}

// Update
void esp8266_update()
{
	uint16_t l = 0;
	uint16_t m_s;
	uint16_t m_l;
	char *b;

	// Check Passthru
	if(esp8266_passthru)																							{ return; }

	// Check First Update
	if(esp8266_first_update)
	{
		// Drop junk
		esp8266_rxbuf_pos = 0;
		esp8266_first_update = 0;
		return;
	}

	// Get Available Length
	l = isafe_rds(&esp8266_rxbuf_pos);

	// Check Message Inbox
	b = (char *)esp8266_rxbuf;

	// Clean up
	m_l = 0;
	while((m_l < l) && ((b[m_l] == '\r') || (b[m_l] == '\n')))														{ m_l = m_l + 1; }
	if(m_l)																											{ esp8266_consume_msg(0, m_l); return; }

	// Get Message
	if(esp8266_has_msg(0, l, &m_s, &m_l) == 0)																		{ return; }
	if(m_l <= 0)																									{ return; }

	// Protect Area (in case we "re-enter" by calling wait_res)
	esp8266_prot_s = m_s;
	esp8266_prot_l = m_l;

	// Check Message (Only handle event messages - IPD / CONNECT / CLOSED)
	if(str_starts_with(&(b[m_s]), m_l, "+IPD,"))																	{ esp8266_handle_ip_data(m_s, m_l); }
	else if(str_starts_with(&(b[m_s + 1]), m_l - 1, ",CONNECT"))													{ esp8266_handle_cnct(m_s, m_l); }
	else if(str_starts_with(&(b[m_s + 1]), m_l - 1, ",CLOSED"))														{ esp8266_handle_dcnt(m_s, m_l); }
	else																											{ /* NoOp */ }

	// Consume Message AND Extra Newlines
	esp8266_consume_msg(0, m_s + m_l);

	// Clear Protection
	esp8266_prot_s = 0;
	esp8266_prot_l = 0;
}

// Hard Reset
void esp8266_reset()
{
	// Disable ESP8266
	dio_lo(esp8266_pin_rst);

	// Delay
	_delay_ms(ESP8266_DELAY_RST);

	// Enable ESP8266
	dio_hi(esp8266_pin_rst);

	// Delay
	_delay_ms(ESP8266_DELAY_RST);
}

// Soft Reset
void esp8266_soft_reset()
{
	// Send Command
	esp8266_send_cmd_f("RST");

	// Delay
	_delay_ms(ESP8266_DELAY_RST);
}

// Enter Flash Mode
void esp8266_enter_flash()
{
	// Re-Init SCOM
	scom_init(esp8266_scom_buf, ESP8266_SCOM_BUFSIZE, esp8266_scom_rx_handler, 0);

	// Enable Flash Mode
	dio_lo(esp8266_pin_flash);

	// Enable Serial Passthrough
	esp8266_passthru = 1;

	// Delay
	_delay_ms(ESP8266_DELAY_RST);

	// Reset
	esp8266_reset();
}

// Enter Passthrough Mode
void esp8266_enter_passthru()
{
	// Re-Init SCOM
	scom_init(esp8266_scom_buf, ESP8266_SCOM_BUFSIZE, esp8266_scom_rx_handler, 0);

	// Enable Serial Passthrough
	esp8266_passthru = 1;

	// Delay
	_delay_ms(ESP8266_DELAY_RST);

	// Reset
	esp8266_reset();
}

// Has Message? (Returns next)
uint8_t esp8266_has_msg(uint16_t start, uint16_t len, uint16_t *nxt_start, uint16_t *nxt_len)
{
	uint16_t p;
	uint16_t d_start;
	uint16_t d_len;
	uint8_t f;
	char *b;

	// Check Length
	if(len == 0)																									{ return 0; }

	// Start
	p = start;
	if(p < (esp8266_prot_s + esp8266_prot_l))																		{ p = esp8266_prot_s + esp8266_prot_l; }
	b = (char *)esp8266_rxbuf;

	// Skip Initial New Line(s)
	f = 1;
	while((p < len) && (f))																							{ if((b[p] == '\r') || (b[p] == '\n')) { p = p + 1; } else { f = 0; } }
	if(p >= len)																									{ return 0; }

	// Handle Send Prompt
	if((b[p] == '>') && (esp8266_sending))																			{ *nxt_start = p; *nxt_len = 1; return 1; }

	// Check for IP Data
	if((len - p) >= 10)
	{
		// Chcek IPD Command
		if(memcmp((char *)(&b[p]), "+IPD,", 5) == 0)
		{
			// Acquire Length
			d_len = atoi((char *)&(b[p + 7]));

			// Acquire Data
			d_start = str_find(&(b[p + 8]), len - (p + 8), ":", 0);
			if(d_start < 0)																							{ return 0; }
			d_start = d_start + 8 + 1;
			if((p + d_start + d_len) > len)																			{ return 0; }

			// Found Valid IPD Message
			*nxt_start = p;
			*nxt_len = d_start + d_len;
			return 1;
		}
	}

	// Find Message
	f = 1;
	d_len = 0;
	while(((p + d_len) < len) && (f))																				{ if((b[p + d_len] == '\r') || (b[p + d_len] == '\n')) { f = 0; } else { d_len = d_len + 1; } }
	if((f) || ((p + d_len) > len))																					{ return 0; }

	// Message Found
	*nxt_start = p;
	*nxt_len = d_len;
	return 1;
}

// Consume Message
void esp8266_consume_msg(uint16_t start, uint16_t len)
{
	uint16_t i;

	// Check Size
	if(len == 0)																									{ return; }

	// Lock Interrupts
	cli();

	// Slice RX Buffer
	if((start + len) > esp8266_rxbuf_pos)																			{ len = esp8266_rxbuf_pos - start; }
	for(i = start; i < (esp8266_rxbuf_pos - len); i = i + 1)														{ esp8266_rxbuf[i] = esp8266_rxbuf[i + len]; }

	// Update Position
	esp8266_rxbuf_pos = esp8266_rxbuf_pos - len;

	// Release Interrupts
	sei();
}

// Wait for Command completion
uint8_t esp8266_wait_for_res()
{
	// Wait
	return esp8266_wait_for_res_drop_events(0xff);
}

// Wait for Command completion while dropping channel events
 uint8_t esp8266_wait_for_res_drop_events(uint8_t chan)
{
	uint16_t p;
	uint16_t l;
	uint16_t m_s;
	uint16_t m_l;
	uint16_t timer;
	char *b;
	uint8_t old_sending;

	// Wait for response
	timer = 0;
	b = (char *)esp8266_rxbuf;
	old_sending = esp8266_sending;
	while((esp8266_response == ESP8266_RESPONSE_NONE) && (timer < ESP8266_RESPONSE_TIMEOUT) && (old_sending == esp8266_sending))
	{
		// Start
		p = 0;

		// Get Available Length
		l = isafe_rds(&esp8266_rxbuf_pos);
		if(l)
		{
			// Run through Messages
			while(esp8266_has_msg(p, l, &m_s, &m_l))
			{
				// Check Message
				if(str_starts_with(&(b[m_s]), m_l, "+CIFSR:STAMAC"))												{ esp8266_handle_local_mac(m_s, m_l); }
				else if(str_starts_with(&(b[m_s]), m_l, "+CIFSR:APMAC"))											{ esp8266_handle_local_mac(m_s, m_l); }
				else if(str_starts_with(&(b[m_s]), m_l, "+CIFSR:STAIP"))											{ esp8266_handle_local_ip(m_s, m_l); }
				else if(str_starts_with(&(b[m_s]), m_l, "+CIFSR:APIP"))												{ esp8266_handle_local_ip(m_s, m_l); }
				else if(str_starts_with(&(b[m_s]), m_l, "+CIPAP:ip"))												{ esp8266_handle_ap_ip(m_s, m_l); }
				else if(str_starts_with(&(b[m_s]), m_l, "+CWLAP"))													{ esp8266_handle_ap(m_s, m_l); }
				else if(str_starts_with(&(b[m_s]), m_l, "+CIPSTATUS"))												{ esp8266_handle_peer(m_s, m_l); }
				else if(str_starts_with(&(b[m_s]), m_l, "STATUS"))													{ /* NoOp */ }
				else if(str_starts_with(&(b[m_s]), m_l, "UNLINK"))													{ /* NoOp */ }
				else if(str_starts_with(&(b[m_s]), m_l, "no change"))												{ /* NoOp */ }
				else if(str_starts_with(&(b[m_s]), m_l, "busy "))													{ /* NoOp */ }
				else if(str_starts_with(&(b[m_s]), m_l, "WIFI "))													{ /* NoOp */ }
				else if(str_starts_with(&(b[m_s]), m_l, "Recv "))													{ /* NoOp */ }
				else if((b[m_s] == ' ') && (m_l == 1))																{ /* NoOp */ }
				else if(str_starts_with(&(b[m_s]), m_l, ">"))														{ esp8266_sending = 2; }
				else if(str_starts_with(&(b[m_s]), m_l, "SEND OK"))													{ esp8266_sending = 3; }
				else if(str_starts_with(&(b[m_s]), m_l, "OK"))														{ esp8266_response = ESP8266_RESPONSE_OK; }
				else if(str_starts_with(&(b[m_s]), m_l, "ERROR"))													{ esp8266_response = ESP8266_RESPONSE_ERROR; }
				else if(str_starts_with(&(b[m_s]), m_l, "+IPD,") && (b[m_s + 5] == ('0' + chan)))					{ /* NoOp */ }
				else if(str_starts_with(&(b[m_s + 1]), m_l - 1, ",CONNECT") && (b[m_s] == ('0' + chan)))			{ /* NoOp */ }
				else if(str_starts_with(&(b[m_s + 1]), m_l - 1, ",CLOSED") && (b[m_s] == ('0' + chan)))				{ /* NoOp */ }
				else if((b[m_s] >= '1') && (b[m_s] <= '9') && (b[m_s + 1] != ','))									{ esp8266_handle_ap_client(m_s, m_l); }
				else
				{
					// Skip to next message
					p = m_s + m_l;
					m_l = 0;
				}

				// Consume Message
				if(m_l)
				{
					esp8266_consume_msg(m_s, m_l);
					p = m_s;
					l = l - m_l;
				}
			}
		}

		// Loop
		timer = timer + 1;
		_delay_ms(ESP8266_MSG_PROCESS_DELAY);
	}

	// Check Response
	return esp8266_response != ESP8266_RESPONSE_OK;
}

// Set Mode
uint8_t esp8266_set_mode(uint8_t mode)
{
	// Send Command
	esp8266_send_cmd_f("CWMODE=%i", mode);

	// Wait for response
	return esp8266_wait_for_res();
}

// Set Mux
uint8_t esp8266_set_mux(uint8_t mux)
{
	// Send Command
	esp8266_send_cmd_f("CIPMUX=%i", mux);

	// Wait for response
	return esp8266_wait_for_res();
}

// List Access Points
uint8_t esp8266_list_ap(struct esp8266_ap *ap_list, uint8_t ap_list_size, uint8_t *count)
{
	uint8_t r;

	// Set AP List
	esp8266_ap_list = ap_list;
	esp8266_ap_list_size = ap_list_size;
	esp8266_ap_list_count = 0;

	// Send command
	esp8266_send_cmd("CWLAP");

	// Wait for response
	r = esp8266_wait_for_res();

	// Clear AP List
	esp8266_ap_list = 0;
	esp8266_ap_list_size = 0;
	if(count)																										{ *count = esp8266_ap_list_count; }

	return r;
}

// Join Access Point
uint8_t esp8266_join_ap(void *name, void *password)
{
	uint8_t password_l = 0;

	// Join AP
	if(password)																									{ password_l = strlen(password); }
	return esp8266_join_ap_n(name, strlen(name), password, password_l);
}

// Join Access Point - Fixed Length
uint8_t esp8266_join_ap_n(void *name, uint8_t name_l, void *password, uint8_t password_l)
{
	// Send command
	esp8266_send_cmd_f("CWJAP=\"%t\",\"%t\"", name, name_l, password, password_l);

	// Wait for response
	return esp8266_wait_for_res();
}

// Create Access Point
uint8_t esp8266_create_ap(void *name, void *password, uint8_t sec_type, uint8_t wchan)
{
	uint8_t password_l = 0;

	// Create AP
	if(password)																									{ password_l = strlen(password); }
	return esp8266_create_ap_n(name, strlen(name), password, password_l, sec_type, wchan);
}

// Create Access Point - Fixed Length
uint8_t esp8266_create_ap_n(void *name, uint8_t name_l, void *password, uint8_t password_l, uint8_t sec_type, uint8_t wchan)
{
	// Send command
	esp8266_send_cmd_f("CWSAP=\"%t\",\"%t\",%i,%i", name, name_l, password, password_l, wchan, sec_type);

	// Wait for response
	return esp8266_wait_for_res();
}

// List AP Clients
uint8_t esp8266_list_ap_clients(struct esp8266_ap_client *client_list, uint8_t client_list_size, uint8_t *count)
{
	uint8_t r;

	// Set AP Client List
	esp8266_ap_client_list = client_list;
	esp8266_ap_client_list_size = client_list_size;
	esp8266_ap_client_list_count = 0;

	// Send command
	esp8266_send_cmd("CWLIF");

	// Wait for response
	r = esp8266_wait_for_res();

	// Clear AP Client List
	esp8266_ap_client_list = 0;
	esp8266_ap_client_list_size = 0;
	if(count)																										{ *count = esp8266_ap_client_list_count; }

	return r;
}

// Set AP IP Address
uint8_t esp8266_set_ap_addr(void *ip)
{
	// Set Addr
	return esp8266_set_ap_addr_n(ip, strlen(ip));
}

// Set AP IP Address - Fixed Length
uint8_t esp8266_set_ap_addr_n(void *ip, uint8_t ip_l)
{
	// Send command
	esp8266_send_cmd_f("CIPAP=\"%t\"", ip, ip_l);

	// Wait for response
	return esp8266_wait_for_res();
}

// Get AP IP Address
uint8_t esp8266_get_ap_addr(char *ip)
{
	uint8_t r;

	// Set Local Buffer
	if(ip)																											{ ip[0] = 0; }
	esp8266_local_ip_buf = ip;

	// Send command
	esp8266_send_cmd("CIPAP?");

	// Wait for response
	r = esp8266_wait_for_res();

	// Clear Local Buffer
	esp8266_local_ip_buf = 0;

	return r;
}

// Get Local MAC & IP Address (MAC buffer must be 6 bytes, IP Buffer must be at least 16 bytes)
uint8_t esp8266_get_local_info(uint8_t *mac, char *ip)
{
	uint8_t r;

	// Set Local Buffers
	if(ip)																											{ ip[0] = 0; }
	if(mac)																											{ for(r = 0; r < NET_ETH_ADDR_LEN; r = r + 1) { mac[r] = 0; } }
	esp8266_local_mac_buf = mac;
	esp8266_local_ip_buf = ip;

	// Send command
	esp8266_send_cmd("CIFSR");

	// Wait for response
	r = esp8266_wait_for_res();

	// Clear Local Buffers
	esp8266_local_ip_buf = 0;
	esp8266_local_mac_buf = 0;

	return r;
}

// Open UDP (Socket Interface)
uint8_t esp8266_iface_open_udp(void *ifdata, struct socket **s, char *host, uint8_t host_l, uint16_t port)
{
	uint8_t i;
	uint8_t r;

	// Check Server Socket (Disallow connects while server is active)
	if(esp8266_server_socket.iface)																					{ return 1; }

	// Allocate Channel
	i = 0;
	r = 1;
	while((i < ESP8266_CHANNELS) && (r))																			{ if(esp8266_sockets[i].iface) { i = i + 1; } else { r = 0; } }
	if(i >= ESP8266_CHANNELS)																						{ return 1; }

	// Send Command
	esp8266_send_cmd_f("CIPSTART=%i,\"%s\",\"%t\",%i,%i,2", i, "UDP", host, host_l, port, port);

	// Wait for response
	if(esp8266_wait_for_res_drop_events(i))																			{ return 1; }

	// Set Socket
	esp8266_sockets[i].id = i;
	*s = &(esp8266_sockets[i]);

	return 0;
}

// Connect (Socket Interface)
uint8_t esp8266_iface_cnct(void *ifdata, struct socket **s, char *host, uint8_t host_l, uint16_t port)
{
	uint8_t i;
	uint8_t r;

	// Check Server Socket (Disallow connects while server is active)
	if(esp8266_server_socket.iface)																					{ return 1; }

	// Allocate Channel
	i = 0;
	r = 1;
	while((i < ESP8266_CHANNELS) && (r))																			{ if(esp8266_sockets[i].iface) { i = i + 1; } else { r = 0; } }
	if(i >= ESP8266_CHANNELS)																						{ return 1; }

	// Send Command
	esp8266_send_cmd_f("CIPSTART=%i,\"%s\",\"%t\",%i", i, "TCP", host, host_l, port);

	// Wait for response
	if(esp8266_wait_for_res_drop_events(i))																			{ return 1; }

	// Set Socket
	esp8266_sockets[i].id = i;
	*s = &(esp8266_sockets[i]);

	return 0;
}

// Listen (Socket Interface)
uint8_t esp8266_iface_lstn(void *ifdata, struct socket **s, uint16_t port)
{
	// Check Server Socket (Only allow one at a time)
	if(esp8266_server_socket.iface)																					{ return 1; }

	// Send command
	esp8266_send_cmd_f("CIPSERVER=1,%i", port);

	// Wait for response
	if(esp8266_wait_for_res())																						{ return 1; }

	// Set Server Socket
	*s = &esp8266_server_socket;

	return 0;
}

// Send (Socket Interface)
uint8_t esp8266_iface_send(void *ifdata, struct socket *s, uint8_t *data, uint16_t size)
{
	// Send
	return esp8266_send_channel(s->id, data, size);
}

// Get Peer (Socket Interface)
uint8_t esp8266_iface_get_peer(void *ifdata, struct socket *s, char *ip, uint16_t *port)
{
	uint8_t r;

	// Check Server Socket
	if(s == &esp8266_server_socket)																					{ return 1; }

	// Set Get Peer Data
	esp8266_get_peer_sid = s->id;
	esp8266_get_peer_ip = ip;
	esp8266_get_peer_port = port;

	// Send command
	esp8266_send_cmd_f("CIPSTATUS");

	// Wait for response
	r = esp8266_wait_for_res();

	// Clear Get Peer Data
	esp8266_get_peer_ip = 0;
	esp8266_get_peer_port = 0;

	return r;
}

// Close (Socket Interface)
void esp8266_iface_close(void *ifdata, struct socket *s)
{
	// Check Server Socket
	if(s == &esp8266_server_socket)
	{
		// Send command
		esp8266_send_cmd_f("CIPSERVER=0");

		// Wait for response & trigger soft-reset
		esp8266_wait_for_res();
		esp8266_soft_reset();
		return;
	}

	// Send command
	esp8266_send_cmd_f("CIPCLOSE=%i", s->id);

	// Wait for response
	esp8266_wait_for_res_drop_events(s->id);

	// Clear Socket
	s->id = 0;
}

// Send data over channel
uint8_t esp8266_send_channel(uint8_t channel, void *x, uint16_t s)
{
	uint8_t r;

	// Send command & Wait for response
	esp8266_send_cmd_f("CIPSEND=%i,%i", channel, s);
	if(esp8266_wait_for_res())																						{ return 1; }

	// Wait for send indicator
	esp8266_sending = 1;
	esp8266_response = ESP8266_RESPONSE_NONE;
	esp8266_wait_for_res();
	if(esp8266_sending != 2)																						{ esp8266_reset(); return 1; }

	// Send Data
	esp8266_send(x, s);


	// Wait for response
	esp8266_response = ESP8266_RESPONSE_NONE;
	esp8266_wait_for_res();
	r = esp8266_sending;
	esp8266_sending = 0;
	if(r != 3)																										{ esp8266_reset(); return 1; }

	return 0;
}

// Send Command - Format
void esp8266_send_cmd_f(char *fmt, ...)
{
	va_list ap;

	// Acquire Args
	va_start(ap, fmt);

	// Clear Response
	esp8266_response = ESP8266_RESPONSE_NONE;

	// Send Prefix
	esp8266_send((uint8_t *)"AT+", 3);

	// Print
	cvprintf(esp8266_send_cmd_f_print, 0, fmt, ap);

	// Release Args
	va_end(ap);

	// Terminate Line
	esp8266_write('\r');
	esp8266_write('\n');
}

// Format String Printer
void esp8266_send_cmd_f_print(void *user, uint8_t c)
{
	// Wait for TX Ready
	while((UCSR1A & (1 << UDRE1)) == 0);

	// TX
	UDR1 = c;
}

void esp8266_send_cmd(char *cmd)
{
	uint16_t l;

	// Acquire Command Length
	l = strlen(cmd);

	// Clear Response
	esp8266_response = ESP8266_RESPONSE_NONE;

	// Send Prefix
	esp8266_send((uint8_t *)"AT+", 3);

	// Send Command Payload
	esp8266_send((uint8_t *)cmd, l);

	// Terminate Line
	esp8266_write('\r');
	esp8266_write('\n');
}

// Send Data
void esp8266_send(uint8_t *d, uint16_t s)
{
	uint16_t i;

	// Loop
	for(i = 0; i < s; i = i + 1)
	{
		// Wait for TX Ready
		while((UCSR1A & (1 << UDRE1)) == 0);

		// TX
		UDR1 = d[i];
	}
}

// Send Byte
void esp8266_write(uint8_t d)
{
	// Wait for TX Ready
	while((UCSR1A & (1 << UDRE1)) == 0);

	// TX
	UDR1 = d;
}

// Handle Access Point Information
void esp8266_handle_ap(uint16_t s, uint16_t l)
{
	uint8_t *p;
	uint8_t sec_type;
	uint8_t ssid_start;
	uint8_t ssid_l;
	uint8_t q_start;
	uint8_t mac_start;
	uint8_t mac_l;
	uint8_t chan_start;
	struct esp8266_ap *ap;

	// Check List
	if(l > 255)																										{ return; }
	if(esp8266_ap_list == 0)																						{ return; }
	if(esp8266_ap_list_count >= esp8266_ap_list_size)																{ return; }

	// Acquire Security Type
	p = &(esp8266_rxbuf[s]);
	sec_type = p[8] - '0';

	// Acquire SSID (Name)
	ssid_start = 11;
	if(ssid_start < l)
	{
		// Acquire SSID Length
		ssid_l = str_find((char *)&(p[ssid_start]), l - ssid_start, "\"", 0);
		if(ssid_l <= 0)																								{ return; }

		// Acquire RSSI (Quality)
		q_start = ssid_start + ssid_l + 2;
		if(q_start < l)
		{
			// Acquire MAC Address
			mac_start = str_find((char *)&(p[q_start]), l - q_start, "\"", 0);
			if(mac_start <= 0)																						{ return; }
			mac_start = mac_start + q_start;
			if(mac_start < (l - 2))
			{
				mac_start = mac_start + 1;
				mac_l = str_find((char *)&(p[mac_start]), l - mac_start, "\"", 0);
				if(mac_l <= 0)																						{ return; }

				// Acquire Channel
				chan_start = mac_start + mac_l + 2;
				if(chan_start < (l - 1))
				{
					// Set in list
					ap = &(esp8266_ap_list[esp8266_ap_list_count]);
					ap->name_len = (ssid_l > NET_WIFI_AP_NAME_LEN) ? NET_WIFI_AP_NAME_LEN : ssid_l;
					memcpy(ap->name, &(p[ssid_start]), ap->name_len);
					ap->sec_type = sec_type;
					ap->chan = atoi((char *)&(p[chan_start]));
					ap->q = atoi((char *)&(p[q_start]));
					esp8266_ap_list_count = esp8266_ap_list_count + 1;
				}
			}
		}
	}
}

// Handle Access Point Client Information
void esp8266_handle_ap_client(uint16_t s, uint16_t l)
{
	uint8_t *p;
	int16_t i;
	struct esp8266_ap_client *c;

	// Check List
	if(l > 255)																										{ return; }
	if(esp8266_ap_client_list == 0)																					{ return; }
	if(esp8266_ap_client_list_count >= esp8266_ap_client_list_size)													{ return; }

	// Find Delim
	p = &(esp8266_rxbuf[s]);
	i = str_find((char *)p, l, ",", 0);
	if((i < 3) || (i >= NET_IP_ADDR_LEN_TXT))																		{ return; }

	// Set in list
	c = &(esp8266_ap_client_list[esp8266_ap_client_list_count]);
	memcpy(c->ip, p, i);
	c->ip[i] = 0;
	esp8266_ap_client_list_count = esp8266_ap_client_list_count + 1;
}

// Handle Access Point IP Information
void esp8266_handle_ap_ip(uint16_t s, uint16_t l)
{
	uint8_t m;
	int16_t ip_l;

	// Check IP Buf
	if(esp8266_local_ip_buf == 0)																					{ return; }

	// Acquire IP Start
	m = 11;
	if(m < l)
	{
		// Get IP
		ip_l = str_find((char *)&(esp8266_rxbuf[s + m]), l - m, "\"", 0);
		if((ip_l <= 0) || (ip_l >= NET_IP_ADDR_LEN_TXT))															{ return; }
		if((m + ip_l) < l)
		{
			memcpy(esp8266_local_ip_buf, &(esp8266_rxbuf[s + m]), ip_l);
			esp8266_local_ip_buf[ip_l] = 0;
		}
	}
}

// Handle Peer Info
void esp8266_handle_peer(uint16_t s, uint16_t l)
{
	uint8_t m;
	int16_t ip_l;
	uint8_t chan;

	// Check Length
	if(l > 255)																										{ return; }

	// Acquire Socket ID
	chan = atoi((char *)&(esp8266_rxbuf[s + 11]));

	// Check Socket ID
	if(chan != esp8266_get_peer_sid)																				{ return; }

	// Acquire IP
	m = 20;
	if(m < l)
	{
		// Get IP
		ip_l = str_find((char *)&(esp8266_rxbuf[s + m]), l - m, "\"", 0);
		if((ip_l <= 0) || (ip_l >= NET_IP_ADDR_LEN_TXT))															{ return; }
		if((m + ip_l) < l)
		{
			if(esp8266_get_peer_ip)																					{ memcpy(esp8266_get_peer_ip, &(esp8266_rxbuf[s + m]), ip_l); esp8266_get_peer_ip[ip_l] = 0; }
			if(esp8266_get_peer_port)
			{
				// Get Port
				m = m + ip_l + 2;
				*esp8266_get_peer_port = atoi((char *)&(esp8266_rxbuf[s + m]));
			}
		}
	}

}

// Handle Local MAC Information
void esp8266_handle_local_mac(uint16_t s, uint16_t l)
{
	int16_t m;
	int16_t i;

	// Check MAC Buf
	if(l > 255)																										{ return; }
	if(esp8266_local_mac_buf == 0)																					{ return; }

	// Acquire MAC Start
	m = str_find((char *)&(esp8266_rxbuf[s]), l, "\"", 0);
	if(m <= 0)																										{ return; }
	m = m + 1;
	if(m < l)
	{
		// Loop through MAC bytes
		for(i = 0; i < NET_ETH_ADDR_LEN; i = i + 1)
		{
			// Check
			if(m < l)
			{
				// Read byte
				esp8266_local_mac_buf[i] = strtoul((char *)&(esp8266_rxbuf[s + m]), 0, 16);

				// Move
				while((m < l) && (esp8266_rxbuf[s + m] != '"') && (esp8266_rxbuf[s + m] != ':'))					{ m = m + 1; }
				m = m + 1;
			}
		}
	}
}

// Handle Local IP Information
void esp8266_handle_local_ip(uint16_t s, uint16_t l)
{
	int16_t m;
	int16_t ip_l;

	// Check IP Buf
	if(esp8266_local_ip_buf == 0)																					{ return; }

	// Acquire IP Start
	m = str_find((char *)&(esp8266_rxbuf[s]), l, "\"", 0);
	if(m <= 0)																										{ return; }
	m = m + 1;
	if(m < l)
	{
		// Get IP
		ip_l = str_find((char *)&(esp8266_rxbuf[s + m]), l - m, "\"", 0);
		if((ip_l <= 0) || (ip_l >= NET_IP_ADDR_LEN_TXT))															{ return; }
		if((m + ip_l) < l)
		{
			memcpy(esp8266_local_ip_buf, &(esp8266_rxbuf[s + m]), ip_l);
			esp8266_local_ip_buf[ip_l] = 0;
		}
	}
}

// Handle Connection
void esp8266_handle_cnct(uint16_t s, uint16_t l)
{
	uint8_t channel;
	struct socket *sock;

	// Check Server
	if(esp8266_server_socket.iface == 0)																			{ return; }

	// Acquire Channel
	channel = atoi((char *)&(esp8266_rxbuf[s]));

	// Check Channel
	if((channel < ESP8266_CHANNELS) && (!(esp8266_sockets[channel].iface)))
	{
		// Create Channel
		sock = &(esp8266_sockets[channel]);
		sock->id = channel;
		socket_srv_client_setup(&esp8266_server_socket, sock);
	}
}

// Handle Disconnect
void esp8266_handle_dcnt(uint16_t s, uint16_t l)
{
	uint8_t channel;
	struct socket *sock;

	// Acquire Channel
	channel = atoi((char *)&(esp8266_rxbuf[s]));

	// Check Channel
	if(channel < ESP8266_CHANNELS)
	{
		// Check Channel & Disconnect Handler
		sock = &(esp8266_sockets[channel]);
		socket_teardown(sock);
		sock->id = 0;
	}
}

// Handle IP Data
void esp8266_handle_ip_data(uint16_t s, uint16_t l)
{
	uint8_t chan;
	uint16_t pl;
	int16_t d_start;
	struct socket *sock;

	// Check Available
	if(l < 10)																										{ return; }

	// Acquire Channel
	chan = atoi((char *)&(esp8266_rxbuf[s + 5]));

	// Acquire Length
	pl = atoi((char *)&(esp8266_rxbuf[s + 7]));

	// Acquire Data
	d_start = str_find((char *)&(esp8266_rxbuf[s]), l, ":", 0);
	if(d_start <= 0)																								{ return; }
	d_start = d_start + 1;
	if(d_start >= l)																								{ return; }
	if((d_start + pl) > l)																							{ pl = l - d_start; }

	// Check Channel
	if(chan < ESP8266_CHANNELS)
	{
		// Pass Data to Handler
		sock = &(esp8266_sockets[chan]);
		if(sock->recv_handler)																						{ sock->recv_handler(sock->user, sock, &(esp8266_rxbuf[s + d_start]), pl); }
	}
}

// SCOM RX Handler
void esp8266_scom_rx_handler(uint8_t x)
{
	// Forward to Syphoon
	esp8266_write(x);

	// Clear Buffer
	scom_rxbuf_pos = 0;
}

// UART RX Interrupt Service Routine
ISR(USART1_RX_vect)
{
	uint8_t x;

	// Pull Byte from UART & Clear RXC Flag
	x = UDR1;

	// Handle Passthrough
	if(esp8266_passthru)																							{ scom_write(x); return; }

	// Handle Overflow (Roll over to next line)
	if(esp8266_overflow)
	{
		if(x == '\n')																								{ esp8266_overflow = 0; }
		return;
	}

	// Push into RX Buffer
	if(esp8266_rxbuf_pos < esp8266_rxbuf_size)																		{ esp8266_rxbuf[esp8266_rxbuf_pos] = x; esp8266_rxbuf_pos = esp8266_rxbuf_pos + 1; }
	else																											{ esp8266_overflow = 1; }
}
